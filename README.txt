HTML Sucker
===========

The HTML Sucker module allows site builder to create
Drupal blocks fetching HTML from other websites.
This could be handy if you need static blocks with
content generated externally.


Requirements
------------

This module requires the following modules:
 * simplehtmldom API (https://www.drupal.org/project/simplehtmldom)


How to use it
-------------
When this module is enabled, users can configure new
blocks by going to:
Administration » Configuration » User interface » HTML Sucker Blocks

After configuring where the block must fetch the HTML from,
a new system block will be generated and this can be
placed in any theme region.

Note:
The remote HTML will be fetched the first time the block
is rendered and it will be stored in database.
If you need to re-fetch the HTML you can get back to
Administration » Configuration » User interface » HTML Sucker Blocks
and click on "Save and suck HTML".


Maintainers
-----------
 * Fabrizio Meinero (Sift Digital) - https://www.drupal.org/user/1573964

