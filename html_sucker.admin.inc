<?php

/**
 * @file
 * HTML 2 Block module admin file.
 */

/**
 * Blocks configuaration form
 *
 * @param $form
 * @param $form_state
 * @return mixed
 */
function html_sucker_configuration_form($form, &$form_state) {
  $form['#theme'] = 'system_settings_form';
  $form['#attached']['css'][] = drupal_get_path('module', 'html_sucker') . '/html_sucker.css';

  $blocks_config = variable_get(HTML_SUCKER_BLOCKS_CONFIG_VAR, array());
   _html_sucker_add_blocks_config($form, $form_state, $blocks_config);

  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save and suck HTML')
    ),
  );
  return $form;
}

/**
 * Helper function to create a configuration for each block
 * @param $form
 * @param $form_state
 * @param $blocks_config
 */
function _html_sucker_add_blocks_config(&$form, &$form_state, $blocks_config) {
  $num_blocks = isset($form_state['num_blocks']) ? $form_state['num_blocks'] : count($blocks_config);
  $form['blocks_config'] = array(
    '#type' => 'container',
    '#title' => t('Blocks configuration'),
    '#description' => '',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    // The id in the prefix must match the AJAX submit handlers below.
    '#prefix' => '<div id="html-sucker-blocks-config-wrapper">',
    '#suffix' => '</div>',
    '#tree' => TRUE,
  );
  $b = 0;
  // First the existing block configrations.
  foreach ($blocks_config as $block_config) {
    if ($b >= $num_blocks) {
      break;
    }
    _html_sucker_add_block_config($form['blocks_config'], $b++, $block_config);
  }
  if (!isset($form_state['num_blocks'])) {
    $form_state['num_blocks'] = $b;
  }
  // Now add empty block config field sets until we reached num_blocks.
  while ($b < $form_state['num_blocks']) {
    _html_sucker_add_block_config($form['blocks_config'], $b++);
  }

  $form['blocks_config']['add_another'] = array(
    '#value' => empty($form_state['num_blocks']) ? t('Add block configuration') : t('Add another block configuration'),
    '#type' => 'submit',
    '#submit' => array('_html_sucker_add_config_submit'),
    '#ajax' => array(
      'callback' => '_html_sucker_refresh_config_fieldset_js',
      'wrapper' => 'html-sucker-blocks-config-wrapper',
      'effect' => 'fade',
      'speed' => 'fast',
    ),
  );
}

/**
 * Ajax callback in response to new rows.
 *
 * @param array $form
 *   The form.
 * @param array $form_state
 *   The form state.
 *
 * At this point the $form has already been rebuilt. All we have to do here is
 * tell AJAX what part of the form needs to be updated.
 */
function _html_sucker_refresh_config_fieldset_js($form, &$form_state) {
  // Return the updated fieldset, so that ajax.inc can issue commands to the
  // browser to update only the targeted sections of the page.
  return $form['blocks_config'];
}

/**
 * Submit handler for the "Add block" button.
 *
 * @param array $form
 *   The form.
 * @param array $form_state
 *   The form state.
 *
 * Increments the block counter and forces a form rebuild.
 */
function _html_sucker_add_config_submit($form, &$form_state) {
  $form_state['num_blocks']++;
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit handler for the "Remove product" button.
 *
 * @param array $form
 *   The form.
 * @param array $form_state
 *   The form state.
 *
 * Decrements the block counter and forces a form rebuild.
 */
function _html_sucker_remove_config_submit($form, &$form_state) {
  $form_state['num_blocks']--;
  $form_state['rebuild'] = TRUE;
}

/**
 * Helper function to add form fields to the block config
 *
 * @param $form
 * @param $id
 * @param array $block_config
 */
function _html_sucker_add_block_config(&$form, $id, $block_config = array()) {
  $form[$id] = array(
    '#type' => 'fieldset',
    '#title' => t('Block configuration #@id', array('@id' => $id + 1)),
    '#collapsible' => TRUE,
    '#collapsed' => !empty($block_config['description']),
    '#attributes' => array(
      'id' => drupal_html_id('block_config_' . $id),
      'class' => array(drupal_html_class('block_config')),
    ),
  );
  if (isset($block_config['description'])) {
    $form[$id]['#title'] .= ' - ' . $block_config['description'];
  }
  $form[$id]['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Block description') . ' *',
    '#size' => 60,
    '#default_value' => isset($block_config['description']) ? $block_config['description'] : '',
    '#description' => t('A brief description of your block. Used on the <a href="@blocks_page" target="_blank">Blocks administration page</a>', array('@blocks_page' => '/admin/structure/block')),
  );
  $form[$id]['source'] = array(
    '#type' => 'textfield',
    '#title' => t('Block source page') . ' *',
    '#size' => 60,
    '#default_value' => isset($block_config['source']) ? $block_config['source'] : '',
    '#description' => t('The remote page where to get the HTML from.'),
  );
  $form[$id]['css_selector'] = array(
    '#type' => 'textfield',
    '#title' => t('The HTML element') . ' *',
    '#size' => 60,
    '#default_value' => isset($block_config['css_selector']) ? $block_config['css_selector'] : '',
    '#description' => t('Use CSS selectors syntax. <a href="@selectors_ref" target="_blank">CSS Selector Reference</a>', array('@selectors_ref' => 'http://www.w3schools.com/cssref/css_selectors.asp')),
  );
  $form[$id]['prefix'] = array(
    '#type' => 'textarea',
    '#title' => t('Content prefix'),
    '#rows' => 3,
    '#default_value' => isset($block_config['prefix']) ? $block_config['prefix'] : '',
    '#description' => t('Add markup before the content'),
  );
  $form[$id]['suffix'] = array(
    '#type' => 'textarea',
    '#title' => t('Content suffix'),
    '#rows' => 3,
    '#default_value' => isset($block_config['suffix']) ? $block_config['suffix'] : '',
    '#description' => t('Add markup after the content'),
  );
  $form[$id]['attachments'] = array(
    '#type' => 'textarea',
    '#title' => t('HEAD attachments'),
    '#rows' => 3,
    '#default_value' => isset($block_config['attachments']) ? $block_config['attachments'] : '',
    '#description' => t('Anything you add here will be injected in the HEAD tag.'),
  );
  $form[$id]['delta'] = array(
    '#type' => 'hidden',
    '#default_value' => isset($block_config['delta']) ? $block_config['delta'] : _html_sucker_get_valid_delta(),
  );
  if (isset($block_config['description'])) {
    $form[$id]['remove'] = array(
      '#value' => t('Remove block configuration #@id', array('@id' => $id + 1)),
      '#type' => 'submit',
    );
  }
  else{
    $form[$id]['remove'] = array(
      '#value' => t('Remove block configuration #@id', array('@id' => $id + 1)),
      '#type' => 'submit',
      '#submit' => array('_html_sucker_remove_config_submit'),
      '#ajax' => array(
        'callback' => '_html_sucker_refresh_config_fieldset_js',
        'wrapper' => 'html-sucker-blocks-config-wrapper',
        'effect' => 'none',
        'speed' => 'fast',
      ),
    );
  }
}

/**
 * Save the values on the configuration form to the database.
 *
 * @param array $form
 *   The submitted configuration form.
 * @param array $form_state
 *   The submitted configuration form state.
 */
function html_sucker_configuration_form_submit($form, &$form_state) {
  $blocks_config = $form_state['values']['blocks_config'];
  unset($blocks_config['add_another']);
  unset($blocks_config['remove']);
  // If block removed..
  if (substr($form_state['clicked_button']['#value'], 0, 6) === "Remove") {
    $id = str_ireplace('Remove block configuration #', '', $form_state['clicked_button']['#value']);
    unset($blocks_config[$id - 1]);
    drupal_set_message(t('The block configuration have been removed.'));
  }
  else{
    drupal_set_message(t('The block configurations have been saved.'));
  }
  variable_set(HTML_SUCKER_BLOCKS_CONFIG_VAR, $blocks_config);

  // empty fetched html
  variable_set(HTML_SUCKER_BLOCKS_HTML, array());

  // Refresh the cache table or hook_leaflet_block_info() won't be called.
  cache_clear_all('*', 'cache', TRUE);
}

/**
 * Validate the configuration form.
 *
 * @param array $form
 *   The submitted configuration form.
 * @param array $form_state
 *   The submitted configuration form state.
 *
 * The is not a hook implementation. It will be if the underscore prefix is
 * dropped from its name. However it will get called every time "Add" or
 * "Remove" buttons are pressed.
 */
function html_sucker_configuration_form_validate($form, &$form_state) {
  // not validate if is deletion
  if (substr($form_state['clicked_button']['#value'], 0, 6) === "Remove") {
    return;
  }

  // Remove unnecessary elements from the form values.
  form_state_values_clean($form_state);

  $errors = 0;
  foreach ($form_state['values']['blocks_config'] as $id => $block_config) {
    if (empty($block_config['description'])) {
      form_set_error("blocks_config][$id][description", t('Description for block #@id is required.', array('@id' => $id + 1)));
      $errors++;
    }
    if (empty($block_config['source'])) {
      form_set_error("blocks_config][$id][source", t('Source for block #@id is required.', array('@id' => $id + 1)));
      $errors++;
    }
    if (empty($block_config['css_selector'])) {
      form_set_error("blocks_config][$id][css_selector", t('CSS selector for block #@id is required.', array('@id' => $id + 1)));
      $errors++;
    }
  }
  return $errors;
}

/**
 * Helper function to return a valid, not taken delta
 */
function _html_sucker_get_valid_delta() {
  $blocks_config = variable_get(HTML_SUCKER_BLOCKS_CONFIG_VAR, array());
  if (count($blocks_config)) {
    $n = 0;
    do {
      $delta = HTML_SUCKER_DELTA_PREFIX . $n;
      $exists = FALSE;
      foreach ($blocks_config as $b) {
        if ($b['delta']==$delta) {
          $exists = TRUE;
        }
      }
      if (!$exists) {
        return HTML_SUCKER_DELTA_PREFIX . $n;
      }
      $n++;
    } while (TRUE);
  }
  return HTML_SUCKER_DELTA_PREFIX . '0';
}