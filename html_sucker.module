<?php

/*
 * @file
 * "HTML sucker" creates blocks from remote HTML
 */

define('HTML_SUCKER_DELTA_PREFIX', 'html_sucker_block_');
define('HTML_SUCKER_BLOCKS_CONFIG_VAR', 'html_sucker_blocks_config');
define('HTML_SUCKER_BLOCKS_HTML', 'html_sucker_blocks_html');

/**
 * Implements hook_menu().
 */
function html_sucker_menu() {
  $items = array();
  $items['admin/config/user-interface/html-sucker/blocks'] = array(
    'title' => 'HTML Sucker Blocks',
    'description' => 'HTML sucker blocks configuration page',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('html_sucker_configuration_form'),
    'access arguments' => array('administer site'),
    'file' => 'html_sucker.admin.inc',
  );
  return $items;
}

/**
 * Implements hook_block_info().
 *
 * Create a block for each block configuration
 */
function html_sucker_block_info() {
  $block = array();
  $blocks_config = variable_get(HTML_SUCKER_BLOCKS_CONFIG_VAR, array());

  if (count($blocks_config)) {
    foreach ($blocks_config as $id => $b) {
      $block[$b['delta']] = array(
        'info' => t($b['description']),
        'cache' => DRUPAL_NO_CACHE,
      );
    }
  }

  return $block;
}

/**
 * Implements hook_block_view().
 *
 * Fetch content from remote and add it to the block content
 */
function html_sucker_block_view($delta = '')  {
  // if block is not created by html_sucker get out
  if (strpos($delta, HTML_SUCKER_DELTA_PREFIX) === CUBRID_AUTOCOMMIT_FALSE) {
    return;
  }

  // get block configuration
  $block_id = str_ireplace(HTML_SUCKER_DELTA_PREFIX, '', $delta);
  $blocks_config = variable_get(HTML_SUCKER_BLOCKS_CONFIG_VAR, array());
  foreach ($blocks_config as $bc) {
    if ($bc['delta']==$delta) {
      $block_config = $bc;
    }
  }

  // if no config is available delete block
  if (!isset($block_config)) {
    db_delete('block')
      ->condition('delta', $delta)
      ->execute();
    watchdog('html_sucker', 'Block %delta has been deleted.', array('%delta' => $delta), WATCHDOG_INFO);
    return;
  }

  // get remote page if block is not cached
  $blocks_html = variable_get(HTML_SUCKER_BLOCKS_HTML, array());
  if (empty($blocks_html[$delta])) {
    $parse_source = parse_url($block_config['source']);
    $source_base = '//' . $parse_source['host'];
    $html_page = file_get_html($block_config['source']);
    $html_block = $html_page->find($block_config['css_selector'], 0);

    if (!$html_block) {
      return;
    }

    // prepend images src with $source_base
    foreach ($html_block->find("img") as $img) {
      $parse_src = parse_url($img->src);
      if (!isset($parse_src['host'])) {
        $img->src = $source_base . $img->src;
      }
    }

    // prepend <a> href with $source_base
    foreach ($html_block->find("a") as $a) {
      $parse_href = parse_url($a->href);
      if (!isset($parse_href['host'])) {
        $a->href = $source_base . $a->href;
      }
    }

    // Add markup to block content
    $block['content'] = $block_config['prefix'];
    $block['content'] .= $html_block->outertext;
    $block['content'] .= $block_config['suffix'];

    // store it in drpal var for quick usage
    $blocks_html[$delta] = $block['content'];
    variable_set(HTML_SUCKER_BLOCKS_HTML, $blocks_html);
  }
  else{
    $block['content'] = $blocks_html[$delta];
  }

  // Add attachments to head
  $element = array(
    '#type' => 'markup',
    '#markup' => $block_config['attachments'],
  );
  drupal_add_html_head($element, $delta . '-tmpl');

  return $block;
}